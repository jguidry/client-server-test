var path = require('path');
var express = require('express');
var app = express();

var prices = {
  '1': 3455.98,
  '3': 213.23,
  '4': 113.23
}

app
  .use(express.static(path.join(__dirname, 'resources')))
  .get('/', function (req, res) {
    res.sendfile('./resources/index.html');
  })
  .get('/items', function (req, res) {
    res.json([1, 2, 3, 4, 5]);
  })
  .get('/items/:id', function (req, res) {
    var id = req.params.id;
  
    if (id === '1' || id === '3' || id == '4') {
      res.json({ id: id, name: 'A lovely item ' + id + '!', price: prices[id] });
    } else {
      res.status(404).send({ error: 'Not found!' });
    }
  });

var server = app.listen(3000, function () {

  var host = server.address().address;
  var port = server.address().port;

  console.log('Server listening at http://%s:%s', host, port);
});

